/*
 * org.nrg.xft.schema.XFTWebAppSchema
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/1/13 9:13 AM
 */


package org.nrg.xft.schema;

public abstract class XFTWebAppSchema {
	public abstract String toString(String header);
}

