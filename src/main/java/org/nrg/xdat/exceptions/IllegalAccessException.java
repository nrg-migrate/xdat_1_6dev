/*
 * org.nrg.xdat.exceptions.IllegalAccessException
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/1/13 9:13 AM
 */


package org.nrg.xdat.exceptions;

/**
 * @author Tim
 *
 */
@SuppressWarnings("serial")
public class IllegalAccessException extends Exception {
	public IllegalAccessException(String message)
	{
		super(message);
	}
}

