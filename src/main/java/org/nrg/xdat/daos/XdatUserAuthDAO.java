/*
 * org.nrg.xdat.daos.XdatUserAuthDAO
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/1/13 9:13 AM
 */
package org.nrg.xdat.daos;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xdat.entities.XdatUserAuth;
import org.springframework.stereotype.Repository;

@Repository
public class XdatUserAuthDAO extends AbstractHibernateDAO<XdatUserAuth> {

}
